using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tetromino : MonoBehaviour {

	public float fall = 0;
	public float fallSpeed = 1;

	static string getRandomTetromino () {

		int randomTetromino = Random.Range(1, 7);
		string randomTetrominoName = "Prefabs/DiceOne";
		switch (randomTetromino){

			case 1:
				randomTetrominoName = "Prefabs/DiceOne";
				break;
			case 2:
				randomTetrominoName = "Prefabs/DiceTwo";
				break;
			case 3:
				randomTetrominoName = "Prefabs/DiceThree";
				break;
			case 4:
				randomTetrominoName = "Prefabs/DiceFour";
				break;
			case 5:
				randomTetrominoName = "Prefabs/DiceFive";
				break;
			case 6:
				randomTetrominoName = "Prefabs/DiceSix";
				break;
		}

		return randomTetrominoName;

	}
	
	// Update is called once per frame
	void Update() {
		
		CheckUserInput();
	}

	void CheckUserInput() {
		//WHEN RIGHT ARROW IS PRESSED
		if(Input.GetKeyDown(KeyCode.RightArrow)) {
			FindObjectOfType<DiceFall>().moveShapeRight(transform, this);

		//WHEN LEFT ARROW IS PRESSED
		}else if (Input.GetKeyDown(KeyCode.LeftArrow)){
			FindObjectOfType<DiceFall>().moveShapeLeft(transform, this);

				
		//WHEN UP ARROW IS PRESSED
		}else if (Input.GetKeyDown(KeyCode.UpArrow)){
			FindObjectOfType<DiceFall>().rotateShape(transform, this);


		//WHEN DOWN IS PRESSED
		}else if (Input.GetKeyDown(KeyCode.DownArrow) || Time.time - fall >= fallSpeed){
			FindObjectOfType<DiceFall>().letShapeFall(transform, this);
			fall = Time.time;
		}
	}

	public static GameObject random(Vector2 position) {
		return (GameObject) Instantiate(Resources.Load(getRandomTetromino(), typeof(GameObject)), position, Quaternion.identity);
	}

}

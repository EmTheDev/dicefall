﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface DiceFallDelegate {
	void gameDidBegin(DiceFall diceFall);
	void gameDidEnd(DiceFall diceFall);
	void gameShapeDidLand(DiceFall diceFall);
	void gameShapeDidMove(DiceFall diceFall);
	void gameShapeDidDrop(DiceFall diceFall);
	void gameDidLevelUp(DiceFall diceFall);
}

public class DiceFall : MonoBehaviour {

	public DiceFallDelegate diceFallDelegate;

	public GameObject previewTetromino;
	public GameObject nextTetromino;
	private bool gameStarted = false;
	private Vector2 previewTetrominoPosition = new Vector2(-4f, 12);

	public int score = 0;
	public int level = 0;

	public static int gridWidth = 7;
	public static int gridHeight = 13;
	public static Transform[,] grid = new Transform[gridWidth,gridHeight];

	//Temporarily from Tetromino class
	public float fall = 0;
	public float fallSpeed = 1;
	public bool allowRotation = true;
	public bool limitRotation = false;

    //func beginGame() {
	public void beginGame(){

		diceFallDelegate.gameDidBegin(this);

	}

    //func newShape() -> (fallingShape:Shape?, nextShape:Shape?) {
	public void newShape () {

		if(!gameStarted){

			gameStarted = true;

			nextTetromino = Tetromino.random(new Vector2(3.0f, 13.0f));
			previewTetromino = Tetromino.random(previewTetrominoPosition);

			previewTetromino.GetComponent<Tetromino>().enabled = false;

		}else{

			previewTetromino.transform.localPosition = new Vector2(3.0f, 13.0f);
			nextTetromino = previewTetromino;
			nextTetromino.GetComponent<Tetromino>().enabled = true;

			previewTetromino = (GameObject)Instantiate(Resources.Load(getRandomTetromino(), typeof(GameObject)), previewTetrominoPosition, Quaternion.identity);
			previewTetromino.GetComponent<Tetromino>().enabled = false;

		}
	}

    public bool detectIllegalPlacement(Transform transform){
    	foreach(Transform mino in transform) {
			Vector2 pos = FindObjectOfType<DiceFall>().Round (mino.position);
			if (FindObjectOfType<DiceFall>().CheckIsInsideGrid (pos) == false) {
				return false;
			}
			if (FindObjectOfType<DiceFall>().GetTransformAtGridPosition(pos) != null && FindObjectOfType<DiceFall>().GetTransformAtGridPosition(pos).parent != transform){
				return false;
			}
		}
		return true;
    }

	public bool CheckIsInsideGrid(Vector2 pos){
		return ((int)pos.x >= 0 && (int)pos.x < gridWidth && (int)pos.y >= 0);//
	}

   	public void settleShape(Transform transform, Tetromino tetromino){
   		transform.position += new Vector3(0, 1, 0);
		removeCompletedLines();
		if(checkIsAboveGrid(tetromino)){
			endGame();
		}

		tetromino.enabled = false;
		newShape();
   		diceFallDelegate.gameShapeDidLand(this);
   	}

   //func detectTouch() -> Bool {


   	public void endGame(){
   		score = 0;
   		level = 1;
   		diceFallDelegate.gameDidEnd(this);
   	}
        
   // func removeAllBlocks() -> Array<Array<Block>> {

    public void removeCompletedLines(){
		for(int y = 0; y<gridHeight;++y){
			if(isFullRowAt(y)){
				deleteMinoAt(y);
				moveAllRowsDown(y+1);
				--y;
			}
		}
	}

   //func dropShape() {
    

   	public void letShapeFall(Transform transform, Tetromino tetromino){
   		transform.position += new Vector3(0, -1, 0);
   		if (FindObjectOfType<DiceFall>().detectIllegalPlacement(transform)){
			updateGrid(tetromino);
		}else{
			settleShape(transform, tetromino);
   		}
   	}

   	public void rotateShape(Transform transform, Tetromino tetromino){
   		if(allowRotation){
			if (limitRotation){
				if(transform.rotation.eulerAngles.z >= 90){
						transform.Rotate(0 ,0, -90);
				}else{
						transform.Rotate (0, 0, 90);
				}
			}else{
				transform.Rotate (0, 0, 90);
			}
			if (FindObjectOfType<DiceFall>().detectIllegalPlacement(transform)){
				FindObjectOfType<DiceFall>().updateGrid(tetromino);
			}else{
				if(limitRotation){
					if(transform.rotation.eulerAngles.z >= 90){
						transform.Rotate (0, 0, -90);
					}else{
						transform.Rotate (0, 0, 90);
					}
				}else{
					transform.Rotate(0,0,-90);
				}
			}
		}
   	}

   	public void moveShapeLeft(Transform transform, Tetromino tetromino){
   		transform.position += new Vector3(-1, 0, 0);
   		if (detectIllegalPlacement(transform)){
			updateGrid(tetromino);
		}else{
			transform.position += new Vector3(1, 0, 0);
		}
   	}

   	public void moveShapeRight(Transform transform, Tetromino tetromino){
   		transform.position += new Vector3(1, 0, 0);
		if (detectIllegalPlacement(transform)){
			updateGrid(tetromino);
		}else{
			transform.position += new Vector3(-1, 0, 0);
		}
   	}

	public bool isFullRowAt(int y){
		for(int x = 0; x < gridWidth; ++x){
			if(grid[x,y] == null){
				return false;
			}
		}
		return true;
	}

	public bool checkIsAboveGrid(Tetromino tetromino){
		for(int x = 0; x < gridWidth; ++x){
			foreach (Transform mino in tetromino.transform){
				Vector2 pos = Round(mino.position);
				if(pos.y > gridHeight - 2){
					return true;
				}
			}
		}
		return false;
	}

   	public void deleteAllMinos(){
		for (int x = 0; x < gridWidth; ++x){
			for(int y = 0; y < gridHeight; ++y){
				if(grid[x,y] != null){
					Destroy(grid[x,y].gameObject);
					grid[x,y] = null;
				}
			}
		}
	}

	public void deleteMinoAt(int y){
		for (int x = 0; x < gridWidth; ++x){
			
			Destroy(grid[x,y].gameObject);
			grid[x,y] = null;
		
		}
	}

	public void moveRowDown(int y){
		for(int x = 0; x < gridWidth; ++x){
			if (grid[x,y] != null){
				grid[x,y - 1] = grid[x,y];
				grid[x,y] = null;
				grid[x,y-1].position += new Vector3(0,-1,0);
			}
		}
	}

	//Move Rows down one at a time through a loop
	public void moveAllRowsDown(int y){
		for(int i = y; i< gridHeight; ++i){
			moveRowDown(i);
		}
	}

	public void updateGrid (Tetromino tetromino){
		for(int y = 0; y < gridHeight; ++y){

			for(int x = 0; x < gridWidth; ++x){

				if (grid[x,y] != null){

					if (grid[x,y].parent == tetromino.transform){
						grid[x,y] = null;
					}
				}
			}
		}

		foreach (Transform mino in tetromino.transform){
			Vector2 pos = Round (mino.position);
			if (pos.y < gridHeight){
				grid[(int)pos.x, (int)pos.y] = mino;			
			}
		}
	}

	public Transform GetTransformAtGridPosition(Vector2 pos){
		if (pos.y > gridHeight -1){
			return null;
		}else{
			return grid[(int)pos.x, (int)pos.y];
		}
	}

	public Vector2 Round (Vector2 pos){

		return new Vector2 (Mathf.Round(pos.x), Mathf.Round(pos.y));

	}

}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game : MonoBehaviour, DiceFallDelegate {

	DiceFall diceFall;

	void Start () {
		
		diceFall = gameObject.GetComponent<DiceFall>();
		diceFall.diceFallDelegate = this;
		diceFall.beginGame();

	}

	//func didTick() {

	//func nextShape() {
	public void nextShape(DiceFall diceFall){
		diceFall.newShape();
	}

	//func gameDidBegin(swiftris: Swiftris) {
	public void gameDidBegin(DiceFall diceFall){
		if (diceFall.nextTetromino == null){
			this.nextShape(diceFall);
		}else{
			nextShape(diceFall);
		}
	}

	//func gameDidEnd(swiftris: Swiftris) {
	public void gameDidEnd(DiceFall diceFall){
		diceFall.deleteAllMinos();
	}

	public void gameDidLevelUp(DiceFall diceFall){

	}

	public void gameShapeDidLand(DiceFall diceFall){

	}

	public void gameShapeDidMove(DiceFall diceFall){}
	
	public void gameShapeDidDrop(DiceFall diceFall){}
		
}
